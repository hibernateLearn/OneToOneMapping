This is learning app.
I want to learn to use Hibernate.

I followed this tutorial.
https://www.journaldev.com/2882/hibernate-tutorial-for-beginners

Database MySQL
DB tool: MySQL Workbench
Schema hibernateforbegginers

CREATE TABLE `hibernateforbeginners`.`customer` (
  `customer_id` INT NOT NULL,
  `customer_name` VARCHAR(45) NOT NULL,
  `customer_details` VARCHAR(1024) NULL,
  `gender` INT NULL,
  `email_address` VARCHAR(45) NULL,
  `phone_number` VARCHAR(45) NULL,
  PRIMARY KEY (`customer_id`));