package cz.ill_j.hibernate.main;

import cz.ill_j.hibernate.model.Customer;
import cz.ill_j.hibernate.util.Utils;
import org.hibernate.Session;

public class HibernateMain {

    public static void main(String[] args) {

        Customer customer = new Customer();
        customer.setName("RAF_pilot");
        customer.setDetails("this is testing mode");
        customer.setPhone("123456");

        // get session
        Session session = Utils.getSessionFactory().getCurrentSession();
        // start transaction
        session.beginTransaction();
        // save new record
        session.save(customer);
        // commit trasaction
        session.getTransaction().commit();

        System.out.println("Customer ID is: " + customer.getId());

        // close session
        Utils.getSessionFactory().close();
    }
}
