package cz.ill_j.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * javax.persistence.Entity annotation is used to mark a class as Entity bean <br>
 * that can be persisted by hibernate, since hibernate provides JPA implementation.
 */
@Entity
@Table(name="Customer", uniqueConstraints={@UniqueConstraint(columnNames={"ID"})})
public class CustomerJPA {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="customer_ID", nullable=false, unique=true, length=11)
    private int id;

    @Column(name="customer_name", length=20, nullable=false)
    private String name;

    @Column(name="customer_details", length=20)
    private String details;

    @Column(name="gender", length=11)
    private int gender;

    @Column(name="email_address", length=20)
    private String email;

    @Column(name="phone_number", length=20)
    private String phone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
